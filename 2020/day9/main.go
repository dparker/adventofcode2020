package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

type Window struct {
	bottom int
	size int
}

func (w *Window) Next() {
	w.bottom++
}

func (w *Window) Grow() {
	w.size++
}

func (w *Window) ResetSize() {
	w.size = 2
}

func NewWindow(size int) *Window {
	window := &Window{
		bottom: 0,
		size: size,
	}

	return window
}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func augendAddendFor(input []int, total int) (int, int) {
	copyOfSlice := append([]int{}, input...)
	sort.Ints(copyOfSlice)

	for _, augend := range input {
		addend := total - augend

		if contains(copyOfSlice, addend) {
			if addend != augend {
				return addend, augend
			}
		}
	}

	return -1, -1
}

func readInputFile() []int {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []int

	for scanner.Scan() {
		line, _ := strconv.Atoi(scanner.Text())

		lines = append(lines, line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return lines
}

func sumAll(data []int) int {
	sum := 0

	for _, v := range data {
		sum += v
	}

	return sum
}

func getSmallestAndLargest(data []int) (int, int) {
	sort.Ints(data)
	return data[0], data[len(data)-1]
}

func main() {
	lines := readInputFile()

	window := NewWindow(25)

	invalid := -1

	for i := 0; i > -1; window.Next() {
		preamble := lines[window.bottom:window.bottom + window.size]
		augend, _ := augendAddendFor(preamble, lines[window.bottom + window.size])
		if augend == -1 {
			fmt.Println(lines[window.bottom + window.size])
			invalid = lines[window.bottom + window.size]
			break
		}
	}

	fmt.Println(invalid)

	growWindow := NewWindow(1)

	fmt.Println(len(lines))

	for i := range lines {
		sum := 0

		for growWindow.bottom = i; sum <= invalid && (growWindow.bottom + growWindow.size) < len(lines); growWindow.Grow() {
			sum = sumAll(lines[growWindow.bottom:growWindow.bottom + growWindow.size])
			if sum == invalid {
				smallest, largest := getSmallestAndLargest(lines[growWindow.bottom:growWindow.bottom + growWindow.size])
				fmt.Println(smallest, largest)
				fmt.Println(smallest + largest)
				os.Exit(0)
			}
		}
		growWindow.ResetSize()
	}
}
