package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	nop = iota
	acc = iota
	jmp = iota
)

type Instruction struct {
	Instruction int
	Arg int
	Visits int
	Toggle bool
}

func (i *Instruction) Visit(p *Program) error {
	if i.Visits > 0 {
		return fmt.Errorf("instruction has already been visited")
	} else {
		i.Visits++
	}

	return nil
}

func (i *Instruction) Run(p *Program) {
	instructionToRun := p.Instructions[p.InstructionPointer].Instruction

	if i.Toggle {
		if instructionToRun == nop {
			fmt.Printf("nop %+d changed to jmp %+d\n", i.Arg, i.Arg)
			instructionToRun = jmp
		} else if instructionToRun == jmp {
			fmt.Printf("jmp %+d changed to nop %+d\n", i.Arg, i.Arg)
			instructionToRun = nop
		}
	}

	switch instructionToRun {
	case nop:
		p.InstructionPointer++
	case acc:
		p.Accumulator += i.Arg
		p.InstructionPointer++
	case jmp:
		p.InstructionPointer += i.Arg
	}
}

type Program struct {
	Accumulator int
	Instructions []Instruction
	InstructionPointer int
}

func (p *Program) Execute() int {
	for ;p.InstructionPointer < len(p.Instructions); {
		err := p.Instructions[p.InstructionPointer].Visit(p)
		if err != nil {
			return -1
		}

		p.Instructions[p.InstructionPointer].Run(p)
	}

	return 0
}

func (p *Program) Reset() {
	p.Accumulator = 0
	p.InstructionPointer = 0
	for i := range p.Instructions {
		p.Instructions[i].Visits = 0
	}
}

func NewProgram(instructions []Instruction) Program {
	return Program{
		Accumulator:        0,
		Instructions:       instructions,
		InstructionPointer: 0,
	}
}

func parseInstructions(raw []string) []Instruction {
	instructions := make([]Instruction, len(raw))

	for i, rawInstruction := range raw {
		instructionParts := strings.Split(rawInstruction, " ")
		var instruction int

		switch instructionParts[0] {
		case "nop":
			instruction = nop
		case "acc":
			instruction = acc
		case "jmp":
			instruction = jmp
		}

		arg, _ := strconv.Atoi(instructionParts[1])

		instructions[i].Instruction = instruction
		instructions[i].Arg = arg
		instructions[i].Toggle = false
	}

	return instructions
}

func readInputFile() []string {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []string

	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return lines
}

func main() {
	rawInstructions := readInputFile()

	instructions := parseInstructions(rawInstructions)

	program := NewProgram(instructions)

	res := -1
	accum := 0
	for i := 0 ; res != 0 && i < len(instructions); i++ {
		if i > 0 {
			program.Instructions[i-1].Toggle = false
		}

		program.Instructions[i].Toggle = true

		res = program.Execute()

		accum = program.Accumulator

		program.Reset()
	}

	fmt.Println("Accum:", accum)
}
