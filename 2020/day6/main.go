package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func countUniqueCharForAnyone(g []string) int {
	charCount := map[int32]int{}

	for _, person := range g {
		for _, char := range person {
			charCount[char]++
		}
	}

	keys := make([]int32, len(charCount))
	i := 0
	for k := range charCount {
		keys[i] = k
		i++
	}

	return len(keys)
}

func countUniqueCharForEveryone(g []string) int {
	charCount := map[int32]int{}

	for _, person := range g {
		for _, char := range person {
			charCount[char]++
		}
	}

	total := 0

	for _, v := range charCount {
		if v == len(g) {
			total++
		}
	}

	return total
}

func readInputFile() [][]string {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var groups [][]string
	groups = append(groups, []string{})

	for scanner.Scan() {
		line := scanner.Text()

		if len(line) == 0 {
			groups = append(groups, []string{})
		} else {
			groups[len(groups)-1] = append(groups[len(groups)-1], line)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return groups
}

func main() {
	input := readInputFile()

	sumAny := 0
	sumEvery := 0

	for _, group := range input {
		sumAny += countUniqueCharForAnyone(group)
		sumEvery += countUniqueCharForEveryone(group)
	}

	fmt.Println("sumAny:", sumAny)
	fmt.Println("sumEvery:", sumEvery)
}
