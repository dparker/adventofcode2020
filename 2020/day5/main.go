package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"sort"
)

func findRowOrCol(spec string, upperChar string, lowerChar string, total int) int {
	lower := 0
	upper := total - 1
	mid := int(math.Ceil(float64(upper + 1) / float64(2))) - 1

	for _, char := range spec {
		if string(char) == upperChar {
			lower = mid + 1
		} else if string(char) == lowerChar {
			upper = mid
		} else {
			log.Fatal("char was not lower or upper char")
		}

		mid = int(math.Ceil((float64(upper + 1) - float64(lower + 1)) / float64(2))) - 1 + lower
	}

	return lower
}

func readInputFile() []string {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []string

	for scanner.Scan() {
		line := scanner.Text()

		lines = append(lines, line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return lines
}

func main() {
	lines := readInputFile()

	highest := 0

	codes := make([]int, len(lines))

	for i, line := range lines {
		rowSpec := line[:7] // First 7 characters
		colSpec := line[7:] // Last 3 characters

		row := findRowOrCol(rowSpec, "B", "F", 128)
		col := findRowOrCol(colSpec, "R", "L", 8)

		code := row * 8 + col

		if code > highest {
			highest = code
		}

		codes[i] = code
	}

	fmt.Println("Highest:", highest)

	sort.Ints(codes)

	for i, seatCode := range codes {
		if i == 0 || i == len(codes) - 1 {
			continue
		}

		if codes[i+1] - seatCode > 1 {
			fmt.Println("My seat:", seatCode + 1)
		}
	}
}
