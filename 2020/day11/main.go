package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type Spot struct {
	IsFloor bool
	Filled bool
}

func NewSpot(char int32) Spot {
	character := string(char)

	spot := Spot{
		IsFloor: false,
		Filled:  false,
	}

	if character == "." {
		spot.IsFloor = true
	}

	return spot
}

func (spot *Spot) Fill() error {
	if spot.IsFloor {
		return fmt.Errorf("spot is not a seat")
	}

	if spot.Filled {
		return fmt.Errorf("seat already filled")
	}

	spot.Filled = true

	return nil
}

func (spot *Spot) Vacate() error {
	if spot.IsFloor {
		return fmt.Errorf("spot is not a seat")
	}

	if !spot.Filled {
		return fmt.Errorf("seat already vacant")
	}

	spot.Filled = false

	return nil
}

type SeatMap struct {
	Spots [][]Spot
	TotalFilledSpots int
	Width int
	Height int
}

func (sm SeatMap) Next() SeatMap {
	newSpots := make([][]Spot, sm.Height)

	for i := range newSpots {
		newSpots[i] = make([]Spot, sm.Width)
	}

	for i := 0; i < sm.Height; i++ {
		for j := 0; j < sm.Width; j++ {
			occupiedAdjacentSeats := 0

			//#region Adjacent Spots

			// Above-left
			if i > 0 && j > 0 {
				if sm.Spots[i - 1][j - 1].Filled {
					occupiedAdjacentSeats++
				}
			}

			// Directly Above
			if i > 0 {
				if sm.Spots[i - 1][j].Filled {
					occupiedAdjacentSeats++
				}
			}

			// Above-Right
			if i > 0 && j < sm.Width - 1 {
				if sm.Spots[i - 1][j + 1].Filled {
					occupiedAdjacentSeats++
				}
			}

			// Directly Right
			if j < sm.Width - 1 {
				if sm.Spots[i][j + 1].Filled {
					occupiedAdjacentSeats++
				}
			}

			// Bottom-Right
			if i < sm.Height - 1 && j < sm.Width - 1 {
				if sm.Spots[i + 1][j + 1].Filled {
					occupiedAdjacentSeats++
				}
			}

			// Directly Below
			if i < sm.Height - 1 {
				if sm.Spots[i + 1][j].Filled {
					occupiedAdjacentSeats++
				}
			}

			// Bottom-Left
			if i < sm.Height - 1 && j > 0{
				if sm.Spots[i + 1][j - 1].Filled {
					occupiedAdjacentSeats++
				}
			}

			// Directly Left
			if j > 0{
				if sm.Spots[i][j - 1].Filled {
					occupiedAdjacentSeats++
				}
			}

			//#endregion Adjacent Spots

			if !sm.Spots[i][j].IsFloor {
				if !sm.Spots[i][j].Filled && occupiedAdjacentSeats == 0 {
					err := newSpots[i][j].Fill()
					if err != nil {
						log.Fatalln(err)
					}
				}

				if sm.Spots[i][j].Filled && occupiedAdjacentSeats < 4 {
					err := newSpots[i][j].Fill()
					if err != nil {
						log.Fatalln(err)
					}
				}
			} else {
				newSpots[i][j].IsFloor = true
			}
		}
	}

	return NewSeatMap(newSpots)
}

func (sm SeatMap) NextVectored() SeatMap {
	newSpots := make([][]Spot, sm.Height)

	for i := range newSpots {
		newSpots[i] = make([]Spot, sm.Width)
	}

	for i := 0; i < sm.Height; i++ {
		for j := 0; j < sm.Width; j++ {
			if sm.Spots[i][j].IsFloor {
				newSpots[i][j].IsFloor = true
				continue
			}
			occupiedAdjacentSeats := 0

			//#region Adjacent Spots

			// Above-left
			for k := 1; i - k >= 0 && j - k >= 0; k++ {
				spot := sm.Spots[i - k][j - k]
				if !spot.IsFloor && !spot.Filled {
					break
				}

				if spot.Filled {
					occupiedAdjacentSeats++
					break
				}
			}

			// Directly Above
			for k := 1; i - k >= 0; k++ {
				spot := sm.Spots[i - k][j]

				if !spot.IsFloor && !spot.Filled {
					break
				}

				if spot.Filled {
					occupiedAdjacentSeats++
					break
				}
			}

			// Above-Right
			for k := 1; i - k >= 0 && j + k < sm.Width; k++ {
				spot := sm.Spots[i - k][j + k]

				if !spot.IsFloor && !spot.Filled {
					break
				}

				if spot.Filled {
					occupiedAdjacentSeats++
					break
				}
			}

			// Directly Right
			for k := 1; j + k < sm.Width; k ++ {
				spot := sm.Spots[i][j + k]

				if !spot.IsFloor && !spot.Filled {
					break
				}

				if spot.Filled {
					occupiedAdjacentSeats++
					break
				}
			}

			// Bottom-Right
			for k := 1; i + k < sm.Height && j + k < sm.Width; k++ {
				spot := sm.Spots[i + k][j + k]

				if !spot.IsFloor && !spot.Filled {
					break
				}

				if spot.Filled {
					occupiedAdjacentSeats++
					break
				}
			}

			// Directly Below
			for k := 1; i + k < sm.Height; k++ {
				spot := sm.Spots[i + k][j]

				if !spot.IsFloor && !spot.Filled {
					break
				}

				if spot.Filled {
					occupiedAdjacentSeats++
					break
				}
			}

			// Bottom-Left
			for k := 1; i + k < sm.Height && j - k >= 0; k++ {
				spot := sm.Spots[i + k][j - k]

				if !spot.IsFloor && !spot.Filled {
					break
				}

				if spot.Filled {
					occupiedAdjacentSeats++
					break
				}
			}

			// Directly Left
			for k := 1; j - k >= 0; k++ {
				spot := sm.Spots[i][j - k]

				if !spot.IsFloor && !spot.Filled {
					break
				}

				if spot.Filled {
					occupiedAdjacentSeats++
					break
				}
			}

			//#endregion Adjacent Spots

			if !sm.Spots[i][j].IsFloor {
				if !sm.Spots[i][j].Filled && occupiedAdjacentSeats == 0 {
					err := newSpots[i][j].Fill()
					if err != nil {
						log.Fatalln(err)
					}
				}

				if sm.Spots[i][j].Filled && occupiedAdjacentSeats < 5 {
					err := newSpots[i][j].Fill()
					if err != nil {
						log.Fatalln(err)
					}
				}
			}
		}
	}

	return NewSeatMap(newSpots)
}

func NewSeatMap(spots [][]Spot) SeatMap {
	filledSpots := 0

	for _, spotLine := range spots {
		for _, spot := range spotLine {
			if spot.Filled {
				filledSpots++
			}
		}
	}

	seatMap := SeatMap{
		Spots: spots,
		TotalFilledSpots: filledSpots,
		Width: len(spots[0]),
		Height: len(spots),
	}

	return seatMap
}

func renderMap(sm SeatMap) {
	for i := 0; i < sm.Height; i++ {
		for j := 0; j < sm.Width; j++ {
			if sm.Spots[i][j].IsFloor {
				fmt.Print(".")
			} else if sm.Spots[i][j].Filled {
				fmt.Print("#")
			} else {
				fmt.Print("L")
			}

		}
		fmt.Printf("\n")
	}

	fmt.Print("\n\n")
}

func readAndParseInputFile() SeatMap {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var spots [][]Spot

	for scanner.Scan() {
		line := scanner.Text()

		var spotLine []Spot

		for _, char := range line {
			spot := NewSpot(char)
			spotLine = append(spotLine, spot)
		}

		spots = append(spots, spotLine)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	sm := NewSeatMap(spots)

	return sm
}

func main() {
	seatMap := readAndParseInputFile()

	lastCount := -1

	renderMap(seatMap)
	for thisCount := 0; lastCount != thisCount; {
		seatMap = seatMap.Next()
		lastCount = thisCount
		thisCount = seatMap.TotalFilledSpots
		//renderMap(seatMap)
	}

	fmt.Println("Part 1:", lastCount)

	seatMap = readAndParseInputFile()

	lastCount = -1

	for thisCount := 0; lastCount != thisCount; {
		renderMap(seatMap)
		seatMap = seatMap.NextVectored()
		lastCount = thisCount
		thisCount = seatMap.TotalFilledSpots
	}

	renderMap(seatMap)

	fmt.Println("Part 2:", lastCount)
}
