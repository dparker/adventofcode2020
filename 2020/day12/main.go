package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
)

func readInputFile() []string {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []string

	for scanner.Scan() {
		line := scanner.Text()

		lines = append(lines, line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return lines
}

type NavComputer struct {
	NorthSouthPosition int
	EastWestPosition int
	CurrentBearing int
}

func NewNavComputer() *NavComputer {
	return &NavComputer{
		CurrentBearing: East,
		NorthSouthPosition: 0,
		EastWestPosition: 0,
	}
}

func (nc *NavComputer) Apply(command Command) {
	switch command.Bearing {
	case Forward:

		switch nc.CurrentBearing {
		case North:
			nc.NorthSouthPosition += command.Magnitude
		case South:
			nc.NorthSouthPosition -= command.Magnitude
		case East:
			nc.EastWestPosition += command.Magnitude
		case West:
			nc.EastWestPosition -= command.Magnitude
		}

	case Right:
		newBearing := nc.CurrentBearing + command.Magnitude
		turns := newBearing / 90
		singleCircleTurns := turns % 4

		switch singleCircleTurns {
		case 0:
			nc.CurrentBearing = North
		case 1:
			nc.CurrentBearing = East
		case 2:
			nc.CurrentBearing = South
		case 3:
			nc.CurrentBearing = West
		}

	case Left:
		newBearing := nc.CurrentBearing - command.Magnitude
		turns := newBearing / 90
		singleCircleTurns := int(math.Abs(float64(turns))) % 4

		if turns >= 0 {
			switch singleCircleTurns {
			case 0:
				nc.CurrentBearing = North
			case 1:
				nc.CurrentBearing = East
			case 2:
				nc.CurrentBearing = South
			case 3:
				nc.CurrentBearing = West
			}
		} else {
			switch singleCircleTurns {
			case 0:
				nc.CurrentBearing = North
			case 1:
				nc.CurrentBearing = West
			case 2:
				nc.CurrentBearing = South
			case 3:
				nc.CurrentBearing = East
			}
		}


	case North:
		nc.NorthSouthPosition += command.Magnitude
	case South:
		nc.NorthSouthPosition -= command.Magnitude
	case East:
		nc.EastWestPosition += command.Magnitude
	case West:
		nc.EastWestPosition -= command.Magnitude
	}
}

func (nc *NavComputer) GetManhattanDistance() int {
	manhattanDistance := math.Abs(float64(nc.NorthSouthPosition)) + math.Abs(float64(nc.EastWestPosition))
	return int(manhattanDistance)
}

func (nc *NavComputer) String() string {
	var bearing string

	switch nc.CurrentBearing {
	case North:
		bearing = "N"
	case East:
		bearing = "E"
	case West:
		bearing = "W"
	case South:
		bearing = "S"
	}

	return fmt.Sprintf("Bearing: %s, Degrees: %d, NSPosition: %d, EWPosition: %d", bearing, nc.CurrentBearing, nc.NorthSouthPosition, nc.EastWestPosition)
}

const (
	North = 0
	East = 90
	South = 180
	West = 270
	Right = 1
	Left = 2
	Forward = 3
)

type Command struct {
	Bearing int
	Magnitude int
}

func (c Command) String() string {
	return fmt.Sprintf("Bearing: %d, Magnitude: %d", c.Bearing, c.Magnitude)
}

func linesToCommands(lines []string) []Command {
	commands := make([]Command, len(lines))

	for i, line := range lines {
		bearingString := string(line[0])
		magnitude, _ := strconv.Atoi(line[1:])

		var bearing int

		switch bearingString {
		case "F":
			bearing = Forward
		case "R":
			bearing = Right
		case "L":
			bearing = Left
		case "N":
			bearing = North
		case "E":
			bearing = East
		case "S":
			bearing = South
		case "W":
			bearing = West
		}

		commands[i] = Command{
			Bearing: bearing,
			Magnitude: magnitude,
		}
	}

	return commands
}

func main() {
	lines := readInputFile()
	commands := linesToCommands(lines)

	navComputer := NewNavComputer()

	for _, command := range commands {
		navComputer.Apply(command)
		fmt.Println(navComputer)
	}

	fmt.Println(navComputer.GetManhattanDistance())
}
