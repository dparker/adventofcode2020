package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type BagRuleRelationship struct {
	Number int
	Node *BagRuleNode
}

type BagRuleNode struct {
	Color string
	ContainsTextParts []string
	Contains []BagRuleRelationship
	Containers []*BagRuleNode
}

func readAndParseRules() map[string]*BagRuleNode {
	//#region read file
	var ruleStrings []string

	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		rule := scanner.Text()

		ruleStrings = append(ruleStrings, rule)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	//#endregion read file

	//#region first pass
	// First Pass of lines with empty relationships
	rules := make(map[string]*BagRuleNode, len(ruleStrings))

	for _, rule := range ruleStrings {
		rule = strings.ReplaceAll(rule, ".", "")
		rule = strings.ReplaceAll(rule, "bags", "")
		rule = strings.ReplaceAll(rule, "bag", "")

		ruleParts := strings.Split(rule, "contain")

		containerPart := strings.TrimSpace(ruleParts[0])
		containsParts := strings.Split(ruleParts[1], ",")

		rules[containerPart] = &BagRuleNode{
			Color: containerPart,
			ContainsTextParts: containsParts,
			Contains: []BagRuleRelationship{},
			Containers: []*BagRuleNode{},
		}
	}

	//fmt.Println(rules)

	//#endregion first pass

	//#region second pass
	// Second pass to fill in relationships

	for _, rule := range rules {
		for _, containsPart := range rule.ContainsTextParts {
			trimmed := strings.TrimSpace(containsPart)
			parts := strings.Split(trimmed, " ")
			number, _ := strconv.Atoi(parts[0])
			color := strings.Join(parts[1:], " ")

			if rules[color] != nil {
				rule.Contains = append(rule.Contains, BagRuleRelationship{
					Number: number,
					Node: rules[color],
				})
			}

			if rules[color] != nil {
				rules[color].Containers = append(rules[color].Containers, rule)
			}
		}
	}

	//#endregion second pass

	return rules
}

func countUniqueContainers(node *BagRuleNode, bagName string, colorCounter map[string]int) {
	for _, parentNode := range node.Containers {
		if parentNode.Color != bagName {
			colorCounter[parentNode.Color]++
			countUniqueContainers(parentNode, bagName, colorCounter)
		}
	}
}

func countContainingBags(node *BagRuleNode) int {
	count := 0

	for _, bagNode := range node.Contains {
		count += bagNode.Number
		count += bagNode.Number * countContainingBags(bagNode.Node)
	}

	return count
}

func main() {
	rules := readAndParseRules()

	colorCounter := make(map[string]int, len(rules))

	for k := range rules {
		colorCounter[k] = 0
	}

	countUniqueContainers(rules["shiny gold"], "shiny gold", colorCounter)

	uniqueColors := 0

	for _, v := range colorCounter {
		if v > 0 {
			uniqueColors++
		}
	}

	fmt.Println("Unique bags that contain this bag:", uniqueColors)

	fmt.Println("Bags contained in this bag:", countContainingBags(rules["shiny gold"]))
}
