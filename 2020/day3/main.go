package main

import (
	"adventofcode/day3/lib"
	"bufio"
	"fmt"
	"log"
	"os"
)

func parseTerrain(lines []string) [][]bool {
	terrain := make([][]bool, len(lines))
	for i, line := range lines {
		terrainLine := make([]bool, len(line))

		for j, char := range line {
			if char == 46 { // 46 = '.'
				terrainLine[j] = false
			} else {
				terrainLine[j] = true
			}
		}

		terrain[i] = terrainLine
	}

	fmt.Println("Parsed terrain...")

	printTerrain(terrain)

	return terrain
}

func readInputFile() []string {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []string

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return lines
}

func printTerrain(terrain [][]bool) {
	for _, line := range terrain {
		lineText := ""
		for _, b := range line {
			if b {
				lineText = fmt.Sprint(lineText, "#")
			} else {
				lineText = fmt.Sprint(lineText, ".")
			}
		}

		fmt.Println(lineText)
	}
}

func countTreesForSlope(terrain [][]bool, slopeX int, slopeY int) int {
	tc := lib.NewTerrainCursor(terrain, slopeX, slopeY)
	trees := 0

	i := 1

	for ; tc.HasNext(); tc.Next() {
		if tc.IsTree() {
			trees++
		}
		i++
	}

	return trees
}

func main() {
	lines := readInputFile()

	terrain := parseTerrain(lines)

	// Part 1
	trees := countTreesForSlope(terrain, 3, 1)
	fmt.Println("Part 1:", trees)

	// Part 2
	a := countTreesForSlope(terrain, 1, 1)
	b := countTreesForSlope(terrain, 3, 1)
	c := countTreesForSlope(terrain, 5, 1)
	d := countTreesForSlope(terrain, 7, 1)
	e := countTreesForSlope(terrain, 1, 2)

	fmt.Println("Part 2:", a * b * c * d * e)
}
