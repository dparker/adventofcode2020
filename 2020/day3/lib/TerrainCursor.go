package lib

type TerrainCursor struct {
	terrain [][]bool
	CurrentX int
	CurrentY int
	xMove int
	yMove int
}

func NewTerrainCursor(terrain [][]bool, xMove int, yMove int) *TerrainCursor {
	terrainCursor := &TerrainCursor{
		terrain: terrain,
		CurrentX: 0,
		CurrentY: 0,
		xMove: xMove,
		yMove: yMove,
	}

	return terrainCursor
}

func (tc *TerrainCursor) Next() *TerrainCursor {
	tc.CurrentX += tc.xMove
	tc.CurrentY += tc.yMove

	if tc.CurrentX >= len(tc.terrain[tc.CurrentY]) {
		tc.CurrentX = tc.CurrentX - len(tc.terrain[tc.CurrentY])
	}

	return tc
}

func (tc *TerrainCursor) HasNext() bool {
	if tc.CurrentY + 1 >= len(tc.terrain) {
		return false
	}

	return true
}

func (tc *TerrainCursor) IsTree() bool {
	return tc.terrain[tc.CurrentY][tc.CurrentX]
}
