package main

import (
	"bufio"
	"fmt"
	"github.com/gookit/validate"
	"log"
	"os"
	"strconv"
	"strings"
)

type Passport struct {
	Byr int	`validate:"required|int|min:1920|max:2002"`
	Iyr int	`validate:"required|int|min:2010|max:2020"`
	Eyr int	`validate:"required|int|min:2020|max:2030"`
	Hgt string	`validate:"required|heightValidator"`
	Hcl string	`validate:"required|hairColorValidator"`
	Ecl string	`validate:"required|in:amb,blu,brn,gry,grn,hzl,oth"`
	Pid string	`validate:"required|len:9"`
	Cid string
}

func (p Passport) HeightValidator(val string) bool {
	if len(val) == 0 {
		return false
	}

	switch val[len(val)-2:] {
	case "cm":
		numString := val[:len(val)-2]
		num, _ := strconv.Atoi(numString)
		if num >= 150 && num <= 193 {
			return true
		}
		return false
	case "in":
		numString := val[:len(val)-2]
		num, _ := strconv.Atoi(numString)
		if num >= 59 && num <= 76 {
			return true
		}
		return false
	}

	return false
}

func (p Passport) HairColorValidator(val string) bool {
	if len(val) != 7 {
		return false
	}

	firstChar := val[:1]
	if firstChar != "#" {
		return false
	}

	rest := val[1:]
	if validate.Regexp(rest, "^[a-z0-9]+$") {
		return true
	}

	return false
}

func readInputFile() []string {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []string
	lines = append(lines, "")

	for scanner.Scan() {
		line := scanner.Text()

		if len(line) == 0 {
			lines = append(lines, "")
		} else {
			newLine := strings.ReplaceAll(line, " ", ",")
			if len(lines[len(lines) - 1]) == 0 {
				lines[len(lines) - 1] = fmt.Sprintf("%s", newLine)
			} else {
				lines[len(lines) - 1] = fmt.Sprintf("%s,%s", lines[len(lines) - 1], newLine)
			}
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return lines
}

func processLines(lines []string) []Passport {
	var passports []Passport

	for _, line := range lines {
		var p Passport
		parts := strings.Split(line, ",")
		for _, part := range parts {
			splitPart := strings.Split(part, ":")

			switch splitPart[0] {
			case "byr":
				p.Byr, _ = strconv.Atoi(splitPart[1])
				break
			case "iyr":
				p.Iyr, _ = strconv.Atoi(splitPart[1])
				break
			case "eyr":
				p.Eyr, _ = strconv.Atoi(splitPart[1])
				break
			case "hgt":
				p.Hgt = splitPart[1]
				break
			case "hcl":
				p.Hcl = splitPart[1]
				break
			case "ecl":
				p.Ecl = splitPart[1]
				break
			case "pid":
				p.Pid = splitPart[1]
				break
			case "cid":
				p.Cid = splitPart[1]
				break
			}
		}

		passports = append(passports, p)
	}

	return passports
}

func main() {
	lines := readInputFile()

	passports := processLines(lines)

	valid := 0

	for _, passport := range passports {
		v := validate.Struct(passport)

		if v.Validate() {
			valid++
		} else {
			fmt.Println(v.Errors)
		}
	}

	fmt.Println(valid)
}
