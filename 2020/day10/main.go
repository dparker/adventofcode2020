package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"sync"
)

type TreeNode struct {
	IsRoot bool
	Jolts int
	Children []*TreeNode
	PathsBelow int
	Lock sync.RWMutex
}

func getChainDifferences(adapterJolts []int) map[int]int {
	sort.Ints(adapterJolts)

	differences := map[int]int{}

	for i := 0; i < len(adapterJolts) - 1; i++ {
		if i == 0 {
			differences[adapterJolts[i]]++
		}

		diff := adapterJolts[i+1] - adapterJolts[i]
		if diff > 3 {
			log.Fatal("Difference greater than 3")
		}

		differences[diff]++
	}

	differences[3]++

	return differences
}

func buildTree(joltsList []int) *TreeNode {
	sort.Ints(joltsList)
	workingList := append([]int{0}, joltsList...)
	workingList = append(workingList, workingList[len(workingList) - 1] + 3)

	fmt.Println(workingList)

	nodes := map[int]*TreeNode{
		0: {
			Jolts: 0,
			IsRoot: true,
			Children: []*TreeNode{},
			PathsBelow: 0,
		},
	}

	for i := 0; i < len(workingList); i++ {
		if nodes[workingList[i]] == nil {
			nodes[workingList[i]] = &TreeNode{
				Jolts: workingList[i],
				Children: []*TreeNode{},
				PathsBelow: 0,
			}
		}

		currentDiff := 0
		for j := i + 1; currentDiff < 3 && j < len(workingList); j++ {
			currentDiff = workingList[j] - workingList[i]
			//fmt.Println(currentDiff)
			if currentDiff <= 3 {
				if nodes[workingList[j]] == nil {
					nodes[workingList[j]] = &TreeNode{
						Jolts: workingList[j],
						IsRoot: false,
						Children: []*TreeNode{},
					}
				}

				nodes[workingList[i]].Children = append(nodes[workingList[i]].Children, nodes[workingList[j]])
			}
		}
	}

	return nodes[0]
}

func walkTree(node *TreeNode, res chan<- int, wg *sync.WaitGroup) {
	if node.IsRoot {
		defer close(res)
	}

	if !node.IsRoot {
		defer wg.Done()
	}

	node.Lock.RLock()
	if node.PathsBelow > 0 {
		res <- node.PathsBelow
		node.Lock.RUnlock()
	} else {
		node.Lock.RUnlock()
		if len(node.Children) == 0 {
			//fmt.Println(newCurrentPath)
			res <- 1
		} else {
			lwg := &sync.WaitGroup{}
			lres := make(chan int, len(node.Children))

			for _, childNode := range node.Children {
				lwg.Add(1)

				go walkTree(childNode, lres, lwg)
			}

			lwg.Wait()

			close(lres)

			pathsBelow := 0

			for paths := range lres {
				pathsBelow += paths
			}

			node.Lock.Lock()
			node.PathsBelow = pathsBelow
			node.Lock.Unlock()

			res <- pathsBelow
		}
	}
}

func readInputFile() []int {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []int

	for scanner.Scan() {
		line, _ := strconv.Atoi(scanner.Text())

		lines = append(lines, line)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return lines
}

func main() {
	lines := readInputFile()

	differenceCounts := getChainDifferences(lines)

	fmt.Println(differenceCounts)

	fmt.Println("Solution 1:", differenceCounts[1] * differenceCounts[3])

	rootNode := buildTree(lines)

	wg := &sync.WaitGroup{}

	res := make(chan int, 1)

	go walkTree(rootNode, res, wg)

	wg.Wait()
	totalPaths := <- res

	fmt.Println("Total Paths:", totalPaths)
}
