package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type PasswordLine struct {
	min int
	max int
	letter string
	password string
}

func lineToObj(line string) PasswordLine {
	parts := strings.Split(line, " ")
	rangeParts := strings.Split(parts[0], "-")
	letterParts := strings.Split(parts[1], ":")
	passwordParts := strings.Split(parts[2], "\n")
	minVal, _ := strconv.Atoi(rangeParts[0])
	maxVal, _ := strconv.Atoi(rangeParts[1])

	pwdLine := PasswordLine{
		min: minVal,
		max: maxVal,
		letter: letterParts[0],
		password: passwordParts[0],
	}

	return pwdLine
}

func processLinesV2(lines []string) int {
	validLines := 0

	for _, line := range lines {
		pwdLine := lineToObj(line)

		indexA := pwdLine.min - 1
		indexB := pwdLine.max - 1

		indexAMatch := pwdLine.password[indexA] == pwdLine.letter[0]
		indexBMatch := pwdLine.password[indexB] == pwdLine.letter[0]

		if indexAMatch != indexBMatch {
			validLines++
		}
	}

	return validLines
}

func processLines(lines []string) int {
	validLines := 0

	for _, line := range lines {
		pwdLine := lineToObj(line)

		count := strings.Count(pwdLine.password, pwdLine.letter)
		if pwdLine.min <= count && count <= pwdLine.max {
			validLines++
		}
	}

	return validLines
}

func main() {
	file, err := os.Open("input.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)

	var lines []string

	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Println(processLines(lines))
	fmt.Println(processLinesV2(lines))
}
